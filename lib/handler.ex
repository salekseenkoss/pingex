defmodule Pingex.Handler do
  require Logger

  use GenServer

  # set up timeout limit
  #
  @timeout 60000

  def start_link do
    GenServer.start_link(__MODULE__, [], [])
  end

  def init(_opts) do
    run()
  end

  defp run() do
    case get_addr do
      :undefined ->
        :timer.sleep 1000
        
        run()
      
      addr ->
        async_send(addr)
        :timer.sleep 300
        
        run()
    end
  end

  defp get_addr do
    Pingex.RedisClient.get_addr
  end

  defp async_send(address) do
    {:ok, pid} = Task.Supervisor.start_link()
    
    Task.Supervisor.async(pid, fn ->
      
      case :gen_icmp.ping(to_charlist(address), timeout: @timeout) do
        [{:ok, _host, _addr, _raddr, {_, _, ttl, rtt}, _} | _] ->
          Logger.info("success ping #{address}, TTL=#{ttl}, time=#{rtt}ms")
          Pingex.Influx.send_data({:ok, address, ttl, rtt})
        
        [{:error, :timeout, _, _} | _] ->
          Logger.error(":gen_icmp timeout error: #{address}")
          Pingex.Influx.send_data({:fail, address, 0, 0})

        _ ->
          Logger.error(":gen_icmp error. Host is unreachable.")
          Pingex.Influx.send_data({:fail, address, 0, 0})
      end

      Pingex.RedisClient.rotate(address)
    end)
  end
end
