# InfluxDB measurement wrapper
#
defmodule Pingex.ICMPSeries do
  use Instream.Series

  series do
    database    "pingex"
    measurement "requests"

    tag :host   # host (ip v4 address)
    tag :state  # state (:ok or :fail)

    field :rtt  # request time
    field :ttl  # time to lime
  end
end
