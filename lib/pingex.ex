defmodule Pingex do
  use Application

  def start(_type, _args) do
    import Supervisor.Spec, warn: false

    children = [
      worker(Pingex.RedisClient, [
        :redis,
        Application.get_env(:pingex, :exredis)[:url],
        Application.get_env(:pingex, :exredis)[:reconnect]
      ]),
      worker(Pingex.Handler, [])
    ]

    # Run InfluxDB connection with supervisor
    #
    Supervisor.start_link([ Pingex.Influx.child_spec ], strategy: :one_for_one)

    opts = [strategy: :one_for_one, name: Pingex.Supervisor]
    
    # Run main process: Pingex.Handler
    # Run Redis connection
    #
    Supervisor.start_link(children, opts)
  end
end
