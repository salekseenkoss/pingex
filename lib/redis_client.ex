defmodule Pingex.RedisClient do
  
  @queue_name "network_address"

  def start_link(name, uri, reconnect) do
    client = Exredis.start_using_connection_string(uri, reconnect)
    true = Process.register(client, name)
    {:ok, client}
  end

  # Push address to tail of list
  #
  def rotate(address) do
    :redis |> Exredis.query(["RPUSH", @queue_name, address])
  end

  # Read addres from head of list
  #
  def get_addr do
    :redis |> Exredis.query(["LPOP", @queue_name])
  end
end
