use Mix.Config

config :pingex, Pingex.Influx,
  database:  "pingex",
  host:      "localhost",
  pool:      [ max_overflow: 5, size: 2 ],
  port:      8086,
  scheme:    "http",
  port_udp:  8089,
  writer:    Instream.Writer.UDP

config :pingex, :exredis,
  url: "redis://@localhost:6379/0",
  reconnect: 5,
  max_queue: :infinity
