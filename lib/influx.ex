defmodule Pingex.Influx do
  use Instream.Connection, otp_app: :pingex

  # wrap icmp response as InfluxDB series;
  # write measurement to Influx via UDP
  #
  def send_data({state, host, ttl, rtt}) do
    series = %Pingex.ICMPSeries{}
    series = %{ series | fields: %{ series.fields | rtt: rtt, ttl: ttl } }
    series = %{ series | tags: %{ series.tags | host: host, state: state } }

    series |>
      Pingex.Influx.write(async: true)
  end
end
